#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int N = 1;
  int *send = (int*) calloc(N, sizeof(int));
  int *recv = (int*) calloc(N, sizeof(int));
  int n;
  for(n=0;n<N;++n){
    send[n] = 1;
    recv[n] = 0;
  }


  /* all ranks have to call this before any rank can 
     return */
  //  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Allreduce (send, recv, N, MPI_INT, MPI_SUM,
		 MPI_COMM_WORLD);  
  
  int m;
  for(m=0;m<N;++m){
    printf("rank %d has %d\n", rank, recv[m]);
  }
  
  MPI_Finalize();
  return 0;
}
