%% plot MPI scaling study

strongData = [ %% MPI size, elapsed
		      1, 0.000908012 %% P, elapsed
		      2, 0.000454307 %% P, elapsed
		      3, 0.000305958 %% P, elapsed
		      4, 0.000229602 %% P, elapsed
		      5, 0.000185137 %% P, elapsed
		      6, 0.000155079 %% P, elapsed
		      7, 0.000133085 %% P, elapsed
		      8, 0.000120046 %% P, elapsed
		      9, 0.000104301 %% P, elapsed
		      10, 9.23491e-05 %% P, elapsed
		      11, 8.48842e-05 %% P, elapsed
		      12, 7.82347e-05 %% P, elapsed
		      13, 7.23481e-05 %% P, elapsed
		      14, 6.78515e-05 %% P, elapsed
		      15, 6.40368e-05 %% P, elapsed
		      16, 6.00696e-05 %% P, elapsed
		      17, 6.14262e-05 %% P, elapsed
		      18, 5.75399e-05 %% P, elapsed
		      19, 5.60999e-05 %% P, elapsed
		      20, 5.31173e-05 %% P, elapsed
		      21, 5.04947e-05 %% P, elapsed
		      22, 4.48847e-05 %% P, elapsed
		      23, 4.75717e-05 %% P, elapsed
		      24, 4.67324e-05 %% P, elapsed
		      25, 4.40979e-05 %% P, elapsed
		      26, 4.29392e-05 %% P, elapsed
		      27, 4.19784e-05 %% P, elapsed
		      28, 4.29392e-05 %% P, elapsed
		      29, 3.95823e-05 %% P, elapsed
		      30, 3.87955e-05 %% P, elapsed
		      31, 3.76058e-05 %% P, elapsed
		      32, 3.73769e-05 %% P, elapsed
	       ];

weakData = [
	    1, 0.000106335; % size, elapsed
	     2, 0.000114202; % size, elapsed
	     3, 0.000125647; % size, elapsed
	     4, 0.000133038; % size, elapsed
	     5, 0.000141859; % size, elapsed
	     6, 0.000135899; % size, elapsed
	     7, 0.00013113; % size, elapsed
	     8, 0.00013113; % size, elapsed
	     9, 0.000132799; % size, elapsed
	     10, 0.000140905; % size, elapsed
	     11, 0.00013876; % size, elapsed
	     12, 0.00014472; % size, elapsed
	     13, 0.000140667; % size, elapsed
	     14, 0.000145435; % size, elapsed
	     15, 0.000182867; % size, elapsed
	     16, 0.000148773; % size, elapsed
	     17, 0.000145674; % size, elapsed
	     18, 0.000182152; % size, elapsed
	     19, 0.000156403; % size, elapsed
	     20, 0.000160933; % size, elapsed
	     21, 0.000169516; % size, elapsed
	     22, 0.00017643; % size, elapsed
	     23, 0.000165701; % size, elapsed
	     24, 0.000174761; % size, elapsed
	     25, 0.000202894; % size, elapsed
	     26, 0.00021553; % size, elapsed
	     27, 0.000169277; % size, elapsed
	     28, 0.000178576; % size, elapsed
	     29, 0.000177145; % size, elapsed
	     30, 0.000169039; % size, elapsed
	     31, 0.000215769; % size, elapsed
	     32, 0.000231743; % size, elapsed
	    ];

%% plot strong scaling
figure(1)
  plot(strongData(:,1), strongData(1,2)./strongData(:,2), 'r-', 'linewidth', 2);
hold on
plot(strongData(:,1), strongData(:,1), 'b-', 'linewidth', 2);
hold off

ha = legend('Strong scaling', 'Perfect strong scaling', 'location', 'southeast');
set(ha, 'fontsize', 20);
set(gcf, 'color', 'w')
xlabel('Number of MPI ranks')
ylabel('Speed up')
set(gca, 'fontsize', 20)
grid on

%% plot weak scaling
figure(2)
plot(weakData(:,1), weakData(1,2)./weakData(:,2), 'r-', 'linewidth', 2);
hold on
plot(weakData(:,1), 1+0*weakData(:,1), 'b-', 'linewidth', 2);
hold off
set(gca, 'ylim', [0 1])
  ha = legend('Weak scaling', 'Perfect weak scaling', 'location', 'southeast');
set(ha, 'fontsize', 20);
set(gcf, 'color', 'w')
xlabel('Number of MPI ranks')
ylabel('Efficiency')
set(gca, 'fontsize', 20)
grid on



