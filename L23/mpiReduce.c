#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int N = 1;
  int *send = (int*) calloc(N, sizeof(int));
  int *recv = (int*) calloc(N, sizeof(int));
  int n;
  for(n=0;n<N;++n){
    send[n] = 1;
    recv[n] = 0;
  }

#if 0
  int Nalive = size;
  int tag = 999;
  while(Nalive>1){

    Nalive /= 2;

    if(rank>=Nalive && rank<2*Nalive){
      int dest = rank-Nalive;
      MPI_Send(send, N, MPI_INT, dest, tag, MPI_COMM_WORLD);
    }

    if(rank<Nalive){
      int source = rank+Nalive;
      MPI_Status status;
      MPI_Recv(recv, N, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
      int n;
      for(n=0;n<N;++n){
	send[n] += recv[n];
      }
    }
  }
#endif
  
  int root = 0;
  MPI_Reduce(send, recv, N, MPI_INT, 
	     MPI_SUM, root, MPI_COMM_WORLD);
  
  
  int m;
  for(m=0;m<N;++m){
    printf("rank %d has %d\n", rank, recv[m]);
  }
  
  MPI_Finalize();
  return 0;
}
