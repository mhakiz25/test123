#!/bin/bash
# number of compute nodes
#SBATCH -N 1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node 32
#SBATCH -t 00:03:00
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH -o foo.out
#SBATCH --exclusive

# Submit this file as a job request with
# sbatch submission.sh

# Change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# Unload all except default modules
module reset

# Load the modules you need
module load  mpich/ge/gcc/64/3.3

## TEST 1
# Compile (this may not be necessary if the program is already built)
mpicc -O3 -o mpiStrongScaling.out mpiStrongScaling.c -lm 

# Print the number of threads for future reference
echo "Running mpiStrongScaling"

# Run the program. Don't forget arguments!
for P in `seq 1 32` 
do
mpiexec -n $P    ./mpiStrongScaling.out  1000000 
done

## TEST 2
# Compile (this may not be necessary if the program is already built)
mpicc -O3 -o mpiWeakScaling.out mpiWeakScaling.c -lm 

# Print the number of threads for future reference
echo "Running mpiWeakScaling"

# Run the program. Don't forget arguments!
for P in `seq 1 32` 
do
mpiexec -n $P    ./mpiWeakScaling.out  1000000
done



# The script will exit whether we give the "exit" command or not.
exit

