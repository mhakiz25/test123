#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int N = 1;
  int *send = (int*) calloc(size, sizeof(int));
  int *recv = (int*) calloc(size, sizeof(int));
  int n;

  srand48(clock());
  for(n=0;n<size;++n){
    send[n] = drand48()*15;
    recv[n] = 0;
  }

  int m, r;
  for(r=0;r<size;++r){
    MPI_Barrier(MPI_COMM_WORLD);
    if(r==rank){
      printf("rank %d sends: ", rank);
      for(m=0;m<size;++m){
	printf(" %02d", send[m]);
      }
      printf("\n");
    }
  }

  

  /* all ranks have to call this before any rank can 
     return */

  MPI_Alltoall(send, 1, MPI_INT,
	       recv, 1, MPI_INT,
	       MPI_COMM_WORLD);
  

  for(r=0;r<size;++r){
    MPI_Barrier(MPI_COMM_WORLD);
    if(r==rank){
      printf("rank %d got: ", rank);
      for(m=0;m<size;++m){
	printf(" %02d", recv[m]);
      }
      printf("\n");
    }
  }
  MPI_Finalize();
  return 0;
}
