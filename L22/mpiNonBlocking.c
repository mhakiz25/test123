
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int N = 100;
  int *send = (int*) calloc(N, sizeof(int));
  int *recv = (int*) calloc(N, sizeof(int));
  int n;
  for(n=0;n<N;++n){
    send[n] = rank;
  }

  int source = (rank+3)%size;
  int dest = (size+rank-3)%size; 
  int tag = 999;

  printf("rank=%d sends to %d and recvs from %d\n", rank, dest, source);
  
  MPI_Request sendRequest;
  MPI_Request recvRequest;

  /* make a request to send the "send" data at some point in the future. we promise not to touch the contents of the send buffer until later */

  MPI_Irecv(recv, N, MPI_INT, source, tag, MPI_COMM_WORLD, &recvRequest);
  
  MPI_Isend(send, N, MPI_INT, dest, tag, MPI_COMM_WORLD, &sendRequest);


  /* At this point we have no idea if the send data left and/or the recv data arrived */

#if 1
  /* latency recovery or "latency hiding" */
  int m=0, k = rank+1;
  for(m=0;m<10;++m){
    k += (m%(k+1));
  }
  printf("k=%d\n", k);
#endif

  /* later */
  MPI_Status status;
  MPI_Wait(&sendRequest, &status);
  MPI_Wait(&recvRequest, &status);
  
  MPI_Finalize();
  return 0;
}
