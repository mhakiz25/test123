#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  int N = 1;
  int *send = (int*) calloc(N, sizeof(int));
  int *recv = (int*) calloc(N, sizeof(int));
  int n;
  for(n=0;n<N;++n){
    send[n] = rank+2;
    recv[n] = send[n];
  }

  int source = (rank+3)%size;
  int dest = (size+rank-3)%size; 
  int tag = 999;

  MPI_Request sendRequest;
  MPI_Request recvRequest;

  /* make a request to send the "send" data at some point in the future. we promise not to touch the contents of the send buffer until later */

  int phase = 0;
  int Nphases = log2(size);
  for(phase=0;phase<Nphases;++phase){
    int Nsend = 1<<phase; /* 2^(phase) */
    
    if(rank < Nsend) {
      dest = rank+Nsend;

      //      printf("Nsend=%d, phase=%d, rank=%d sends to %d\n",
      //	     Nsend, phase, rank, dest);
      
      MPI_Isend(send, N, MPI_INT, dest, tag, MPI_COMM_WORLD, &sendRequest);

      MPI_Status status;
      MPI_Wait(&sendRequest, &status);
    }

    if(rank>=Nsend && rank<2*Nsend){
      source = rank-Nsend;

      //      printf("Nsend=%d, phase=%d, rank=%d recvs from %d\n",
      //	     Nsend, phase, rank, source);
      
      MPI_Irecv(recv, N, MPI_INT, source, tag, MPI_COMM_WORLD, &recvRequest);

      // must wait 
      MPI_Status status;
      MPI_Wait(&recvRequest, &status);

      // before using the data
      int m;
      for(m=0;m<N;++m)
	send[m] = recv[m];
      

    }
  }


  int m;
  for(m=0;m<N;++m){
    printf("rank %d has %d\n", rank, recv[m]);
  }
  
  MPI_Finalize();
  return 0;
}
