#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*

To compile with memory checking:
 gcc -fsanitize=address -fno-omit-frame-pointer -o hw06v3.out hw06v3.c -lm

To compile normally:
 gcc -o hw06v3.out hw06v3.c -lm
*/

void count_letters(const char *str, int *letter_counts, int alphabet_size) {
    for (int i = 0; i < strlen(str); i++) {
        char c = tolower(str[i]);
        if (c >= 'a' && c <= 'a' + alphabet_size - 1) {
            letter_counts[c - 'a']++;
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s dictionary_file\n", argv[0]);
        return 1;
    }

    char *filename = argv[1];
    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Error: could not open file '%s'\n", filename);
        return 1;
    }

    
    int num_words = 0;
    char line[BUFSIZ];
    while (fgets(line, BUFSIZ, file) != NULL) {
      ++num_words;
    }
    fseek(file, 0, SEEK_SET);
    
    char **words = malloc(num_words * sizeof(char *));
    if (!words) {
        printf("Error: could not allocate memory for words\n");
        fclose(file);
        return 1;
    }

    num_words = 0;
    while (fgets(line, BUFSIZ, file) != NULL) {
      words[num_words] = strdup(line);
      if (!words[num_words]) {
        printf("Error: could not allocate memory for word\n");
        for (int i = 0; i < num_words; i++) {
          free(words[i]);
        }
        free(words);
        fclose(file);
        return 1;
      }
      num_words++;
    }
    fclose(file);

    int alphabet_size = 26;
    int *letter_counts = calloc(alphabet_size, sizeof(*letter_counts));
    if (!letter_counts) {
        printf("Error: could not allocate memory for letter_counts\n");
        for (int i = 0; i < num_words; i++) {
            free(words[i]);
        }
        free(words);
        return 1;
    }

    for (int i = 0; i < num_words; i++) {
        count_letters(words[i], letter_counts, alphabet_size);
    }

    int max_count = 0;
    char max_letter = 'a';
    for (int i = 0; i < alphabet_size; i++) {
        if (letter_counts[i] > max_count) {
            max_count = letter_counts[i];
            max_letter = 'a' + i;
        }
    }
    printf("Most frequent letter: %c\n", max_letter);
    printf("Number of words in the dictionary: %d\n", num_words);

    printf("\\begin{tikzpicture}\n");
    printf("\\begin{axis}[\n");
    printf("    ybar,\n");
    printf("    xlabel=Letter,\n");
    printf("    ylabel=Frequency,\n");
    printf("    ymin=0,\n");
    printf("    ymax=%d,\n", max_count);
    printf("    xtick=data,\n");
    printf("    xticklabels={");
    for (int i = 0; i < alphabet_size - 1; i++) {
        printf("%c,", 'a' + i);
    }
    printf("%c}\n", 'a' + alphabet_size - 1);
    printf("]\n");
    printf("\\addplot coordinates {\n");
    for (int i = 0; i < alphabet_size; i++) {
        printf("    (%c, %d)\n", 'a' + i, letter_counts[i]);
    }
    printf("};\n");
    printf("\\end{axis}\n");
    printf("\\end{tikz picture}\n");

    for (int i = 0; i < num_words; i++) {
        free(words[i]);
    }
    free(words);
    free(letter_counts);

    return 0;
}
