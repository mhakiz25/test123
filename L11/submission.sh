#!/bin/bash
# number of compute nodes
#SBATCH -N 1
#SBATCH --ntasks-per-node 1
#SBATCH -t 00:01:00
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH -o foo.out

# Submit this file as a job request with
# sbatch submission.sh

# Change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# Unload all except default modules
module reset

# Load the modules you need
module load gcc

# Compile (this may not be necessary if the program is already built)
gcc -o hw06v3.out hw06v3.c -lm 

# Print the number of threads for future reference
echo "Running hw06v3"

# Run the program. Don't forget arguments!
./hw06v3.out usa2.txt

# The script will exit whether we give the "exit" command or not.
exit

