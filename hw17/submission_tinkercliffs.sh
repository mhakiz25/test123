#!/bin/bash
# number of compute nodes
#SBATCH -N 1
#SBATCH --gres=gpu:ampere:1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node 1
#SBATCH -t 00:30:00
#SBATCH -p a100_normal_q
#SBATCH -A cmda3634_rjh
#SBATCH -o foo.out

# Submit this file as a job request with
# sbatch submission.sh

# Change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# Unload all except default modules
module reset

# Load the modules you need
module load  mpich/ge/gcc/64/3.3
module load  cuda-latest/toolkit
module load libpng

# build LBM code
make cuda

# Print the number of threads for future reference
echo "Running cudaLBM"

# Run the program. Don't forget arguments!
./cudaLBM images/fsm.png 180

# make mp4
module load FFmpeg
ffmpeg -y -r 24 -i bah%06d.png  -b:v 16384k -vf scale=1024:-1 funkyImage.mp4

# The script will exit whether we give the "exit" command or not.
exit

