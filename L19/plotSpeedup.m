%% plot speed ups for Amdahl and Gustafson performance models
P = 1:100;

%% serial part
s = 1-0.8;

AmdahlSpeedUp = 1./(s + (1-s)./P);
GustafsonSpeedUp = s + (1-s)*P;

plot(P, AmdahlSpeedUp, 'b-o', 'linewidth', 2)

hold on
%% perfect speed up
plot(P, P, 'k-', 'linewidth', 2);

%% plot Gustafson model
plot(P, GustafsonSpeedUp, 'r-s', 'linewidth', 2)

hold off

set(gca, 'fontsize', 20)
set(gcf, 'color', 'w')
xlabel('P')
ylabel('Speedup (U_P)')

grid on
