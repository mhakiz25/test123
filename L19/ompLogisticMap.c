
/* for details of the logistic map see
https://en.wikipedia.org/wiki/Logistic_map
*/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int main(int argc, char **argv){

  if(argc<3){
    printf("usage: %s N Nthreads\n", argv[0]);
    exit(-1);
  }

  int N = atoi(argv[1]);
  int Nthreads = atoi(argv[2]);

  // weak scaling - scale work by number of threads
  N = N*Nthreads;
  
  double tic = omp_get_wtime();

  int n;

#pragma omp parallel for num_threads(Nthreads)
  for(n=0;n<N;++n){
    
    const double r = 4.*n/(N-1);
    double x = 0.51;

    int m;
    for(m=0;m<1000;++m){
      x = r*x*(1-x);
    }
    
    if(x<0) printf("got negative x\n");
  }

  double toc = omp_get_wtime();
  printf("%d, %g %%%% Nthreads, elapsed\n", Nthreads, toc-tic); 

  return 0;
}
