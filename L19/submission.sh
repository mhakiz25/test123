#!/bin/bash
# number of compute nodes
#SBATCH -N 1
#SBATCH --cpus-per-task=32
#SBATCH --ntasks-per-node 1
#SBATCH -t 00:01:00
#SBATCH -p normal_q
#SBATCH -A cmda3634_rjh
#SBATCH -o foo.out
#SBATCH --exclusive

# Submit this file as a job request with
# sbatch submission.sh

# Change to the directory from which the job was submitted
cd $SLURM_SUBMIT_DIR

# Unload all except default modules
module reset

# Load the modules you need
module load gcc

# Compile (this may not be necessary if the program is already built)
gcc -O3 -fopenmp -o ompLogisticMap.out ompLogisticMap.c -lm 

# OpenMP settings
export OMP_PROC_BIND=spread
export OMP_DYNAMIC=FALSE
export OMP_PLACES=cores

# Print the number of threads for future reference
echo "Running ompLogisticMap"

# Run the program. Don't forget arguments!
for P in `seq 1 32` 
do
    ./ompLogisticMap 1000000 $P
done

# The script will exit whether we give the "exit" command or not.
exit

