
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  int rank, size;
  
  /* initialize MPI as the first thing */
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  /* print out a message */
  printf("Hello world from MPI rank %d of %d\n",
	 rank, size);
  

  /* finalize MPI */
  MPI_Finalize();
  
  return 0;
}
