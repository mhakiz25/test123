
/* for details of the logistic map see
https://en.wikipedia.org/wiki/Logistic_map
*/

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

/* 

To build:

gcc -o ompLogisticMap ompLogisticMap.c  -lm -fopenmp

Use these OpenMP env settings

OMP_PROC_BIND=spread
OMP_PLACES=cores
OMP_NUM_THREADS=32

to run:

for P in `seq 1 64`; do  ./ompLogisticMap 4000000 $P; done

*/

int main(int argc, char **argv){

  if(argc<3){
    printf("usage: %s N Nthreads\n", argv[0]);
    exit(-1);
  }

  int N = atoi(argv[1]);
  int Nthreads = atoi(argv[2]);

  // to use weak scaling - scale work by number of threads
  // N = N*Nthreads;
  int tid;
#pragma omp parallel num_threads(Nthreads)
  {
    tid = omp_get_thread_num();
  }
  
  double tic = omp_get_wtime();

  int n;

#pragma omp parallel for num_threads(Nthreads)
  for(n=0;n<N;++n){
    
    const double r = 4.*n/(N-1);
    double x = 0.51;

    int m;
    for(m=0;m<1000;++m){
      x = r*x*(1-x);
    }
    
    if(x<0) printf("got negative x\n");
  }

  double toc = omp_get_wtime();
  printf("%d, %g %%%% Nthreads, elapsed\n", Nthreads, toc-tic); 

  return 0;
}
