% no compiler optimization
data =[
1, 16.2194 %% Nthreads, elapsed
2, 8.59932 %% Nthreads, elapsed
3, 6.06918 %% Nthreads, elapsed
4, 4.80303 %% Nthreads, elapsed
5, 4.04759 %% Nthreads, elapsed
6, 3.53889 %% Nthreads, elapsed
7, 3.17648 %% Nthreads, elapsed
8, 2.91256 %% Nthreads, elapsed
9, 2.56161 %% Nthreads, elapsed
10, 2.23652 %% Nthreads, elapsed
11, 2.00067 %% Nthreads, elapsed
12, 1.80525 %% Nthreads, elapsed
13, 1.71337 %% Nthreads, elapsed
14, 1.83452 %% Nthreads, elapsed
15, 1.8044 %% Nthreads, elapsed
16, 1.76873 %% Nthreads, elapsed
17, 1.67793 %% Nthreads, elapsed
18, 1.55771 %% Nthreads, elapsed
19, 1.44674 %% Nthreads, elapsed
20, 1.34063 %% Nthreads, elapsed
21, 1.27522 %% Nthreads, elapsed
22, 1.21765 %% Nthreads, elapsed
23, 1.20925 %% Nthreads, elapsed
24, 1.24966 %% Nthreads, elapsed
25, 1.2872 %% Nthreads, elapsed
26, 1.39153 %% Nthreads, elapsed
27, 1.40526 %% Nthreads, elapsed
28, 1.40188 %% Nthreads, elapsed
29, 1.34877 %% Nthreads, elapsed
30, 1.23931 %% Nthreads, elapsed
31, 1.31033 %% Nthreads, elapsed
32, 1.30162 %% Nthreads, elapsed
33, 1.19705 %% Nthreads, elapsed
34, 1.16471 %% Nthreads, elapsed
35, 1.1823 %% Nthreads, elapsed
36, 1.12546 %% Nthreads, elapsed
37, 1.23596 %% Nthreads, elapsed
38, 1.238 %% Nthreads, elapsed
39, 1.24727 %% Nthreads, elapsed
40, 1.19555 %% Nthreads, elapsed
41, 1.1786 %% Nthreads, elapsed
42, 1.16644 %% Nthreads, elapsed
];

%% compiler optimization 
data = [
1, 10.5094 %% Nthreads, elapsed
2, 5.71332 %% Nthreads, elapsed
3, 4.11673 %% Nthreads, elapsed
4, 3.31125 %% Nthreads, elapsed
5, 2.83158 %% Nthreads, elapsed
6, 2.5178 %% Nthreads, elapsed
7, 2.27677 %% Nthreads, elapsed
8, 2.12585 %% Nthreads, elapsed
9, 1.85188 %% Nthreads, elapsed
10, 1.61879 %% Nthreads, elapsed
11, 1.42974 %% Nthreads, elapsed
12, 1.27613 %% Nthreads, elapsed
13, 1.23473 %% Nthreads, elapsed
14, 1.27568 %% Nthreads, elapsed
15, 1.27923 %% Nthreads, elapsed
16, 1.25868 %% Nthreads, elapsed
17, 1.23085 %% Nthreads, elapsed
18, 1.11559 %% Nthreads, elapsed
19, 1.03105 %% Nthreads, elapsed
20, 0.959675 %% Nthreads, elapsed
21, 0.896515 %% Nthreads, elapsed
22, 0.843498 %% Nthreads, elapsed
23, 0.8527 %% Nthreads, elapsed
24, 0.881766 %% Nthreads, elapsed
25, 0.971391 %% Nthreads, elapsed
26, 1.02626 %% Nthreads, elapsed
27, 1.00254 %% Nthreads, elapsed
28, 0.979959 %% Nthreads, elapsed
29, 0.897072 %% Nthreads, elapsed
30, 0.90388 %% Nthreads, elapsed
31, 0.855743 %% Nthreads, elapsed
32, 0.826412 %% Nthreads, elapsed
33, 0.807564 %% Nthreads, elapsed
34, 0.845649 %% Nthreads, elapsed
35, 0.747598 %% Nthreads, elapsed
36, 0.729603 %% Nthreads, elapsed
37, 0.831808 %% Nthreads, elapsed
38, 0.837099 %% Nthreads, elapsed
39, 0.810168 %% Nthreads, elapsed
40, 0.800108 %% Nthreads, elapsed
41, 0.804073 %% Nthreads, elapsed
42, 0.802402 %% Nthreads, elapsed
];

P = data(:,1);
T = data(:,2);
plot(P, T(1)./T, 'k-+', 'linewidth', 2);
hold on
plot(P, P, 'r-', 'linewidth', 2);
hold off
grid on
xlabel('Number of OpenMP threads', 'fontsize', 20)
ylabel('Speed up', 'fontsize', 20)
set(gcf, 'color', 'w')

%% least squares fit of timing data to T(P) = T0 + W/P
n = length(T);
A = [ones(n,1), 1./P];
coeffs = (A'*A)\(A'*T);
T0 = coeffs(1);
W  = coeffs(2);
hold on
plot(P, (T0+W)./(T0 + W./P), 'm-', 'linewidth', 2 )
hold off

title('Amdahls law: logistic map strong scaling study ', 'FontSize', 20)
ha = legend('Measured speed up', 'Ideal speed up', 'Amdahl model speed up', 'location', 'northwest' );
set(ha, 'fontsize', 20)
