
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv){

  omp_set_num_threads(14);

  int i = 0;

#pragma omp parallel shared(i)
  {
    int threadIndex = omp_get_thread_num();

    printf("hello from thread %d\n", threadIndex);

    struct drand48_data seed;
    double rand = 1.234;

    srand48_r(123456 + threadIndex, &seed);
    
    drand48_r(&seed, &rand);

    printf("thread=%d gets rand=%lf\n", threadIndex, rand);
    
    int randomIndex = drand48()*omp_get_num_threads();

    if(threadIndex == randomIndex){
      printf("thread %d got i=%d\n", threadIndex, i);
    }

    int n;
    for(n=0;n<10000;++n){
      int iread = i;
      int iinc = iread+1;
      i = iinc;
    }
    
  }

  printf("i=%d\n", i);
  

  return 0;
}
