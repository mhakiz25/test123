#include <stdlib.h>
#include <stdio.h>

typedef struct {

  int length;
  int* array;

} intArray_t;

intArray_t intArray(int N){

  intArray_t ia;
  
  ia.length = N;
  ia.array  = (int*) malloc(N*sizeof(int));

  return ia;
}

void intArraySet(intArray_t ia, int index, int val){

  if(ia.array==NULL){
    printf("intArraySet: array pointer is NULL\n");
    exit(-1);
  }

  if(index>=ia.length || index<0){
    printf("intArraySet: out of bounds write\n");
    exit(-1);
  }
  
  ia.array[index] = val;
  
}

void intArrayFree(intArray_t *ia){
  free(ia->array);
  ia->length = 0;
  ia->array = NULL;
}
  

int main(){

  int i = 3; /* reserving 4 bytes on the stack */
  int j = 7; /* another 4 bytes */

  int N = 100;

  intArray_t someVec = intArray(N);
  intArraySet(someVec, 10, 1);
  //  intArraySet(someVec, 100, 2);

  intArray_t* pt = &someVec;
  
  intArrayFree(pt);
  
  intArraySet(someVec, 20, 9);
}
