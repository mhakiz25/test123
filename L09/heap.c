#include <stdlib.h>
#include <stdio.h>

int* intArray(int N){
  int* hvec= (int*) malloc(N*sizeof(int));
  return hvec;
}

void intArraySet(int length, int *array, int index, int val){

  if(array==NULL){
    printf("intArraySet: array pointer is NULL\n");
    exit(-1);
  }

  if(index>=length || index<0){
    printf("intArraySet: out of bounds write\n");
    exit(-1);
  }
  
  array[index] = val;
  
}

int main(){

  int i = 3; /* reserving 4 bytes on the stack */
  int j = 7; /* another 4 bytes */

  int N = 100;

  int* someVec = intArray(N);
  intArraySet(N, someVec, 10, 1);
  intArraySet(N, someVec, 100, 2);
  
  free(someVec);
  someVec = NULL;
  
  intArraySet(N, someVec, 20, 9);
}
