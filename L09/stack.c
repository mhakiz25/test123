#include <stdlib.h>
#include <stdio.h>

int *foo(){

  int kvec[100000];

  return kvec;
}


int main(){

  int i = 3; /* reserving 4 bytes on the stack */
  int j = 7; /* another 4 bytes */
  int ivec[100]; /* another 400 bytes of stack space (hidden is probably an 8 byte pointer to the start of that stack space */

  int *kvec = foo(); /* bad - since kvec is no longer stacked */
  
  /*  int jvec[10000000]; this will crash - the stack cannot hold this since it is ~40MB */
}
