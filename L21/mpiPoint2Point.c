
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv){

  int rank, size;
  
  /* initialize MPI as the first thing */
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int i = rank;
  

  if(rank==0){

    /* send some data to rank 1 */

    int destination = 1; /* rank to send data to */
    int N = 20; /* number of ints to send */
    int *buffer = (int*) calloc(N, sizeof(int));
    int n;

    int tag = 999;

    for(n=0;n<N;++n){
      buffer[n] = n;
    }

    MPI_Send(buffer,
	     N,
	     MPI_INT,
	     destination,
	     tag,
	     MPI_COMM_WORLD);

  }

  if(rank==1){

    /* received some data from rank 0 */

    int source = 0; /* rank to receive data to */
    int N = 20; /* number of ints to send */
    int *buffer = (int*) calloc(N, sizeof(int));
    int n;

    int tag = 999;

    MPI_Status status;
    
    MPI_Recv(buffer,
	     N,
	     MPI_INT,
	     source,
	     tag,
	     MPI_COMM_WORLD,
	     &status);

    for(n=0;n<N;++n){
      printf("buffer[%d]=%d\n", n, buffer[n]);
    }
    
  }
  
  
  /* finalize MPI */
  MPI_Finalize();
  
  return 0;
}
