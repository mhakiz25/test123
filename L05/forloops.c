
#include <math.h>
#include <stdio.h>

double f(int N, double x){

  double sum = 0;
  int n;
  
  for(n=0;n<=N;++n){
    sum = sum +
      pow(-1,n)*pow(x,2*n+1)/gamma(2*n);
  }

  return sum;
}

int main(){

  int n;
  int res = 0;

  double foo = f(100, M_PI/2);

  printf("foo=%lf\n", foo);
  
  return 0;
  
  
}
