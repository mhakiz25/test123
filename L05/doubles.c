
#include <math.h>
#include <stdio.h>

int main(){

  int tom=2000000;
  int jane=3000000;
  int bill = tom*jane;

  printf("bill=%d\n", bill);

  /* 32 bit value */
  float x = 1.2;
  float y = -1.7;
  float z = x*y;

  printf("z=%f\n", z);

  /* 64 bit value */
  double X = 1.2;
  double Y = -1.7;
  double Z = X*Y;

  printf("Z=%lf\n", Z);


  
  return 0;
  
  
}
