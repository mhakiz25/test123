
#include <stdio.h>
#include <stdlib.h>
#include "cuda.h"


__global__ void helloWorld(){

  /* this will execute on the GPU (DEVICE) */
//  printf("Hello world\n");
  printf("hello from thread %d of thread-block %d\n",
         threadIdx.x, blockIdx.x);
  
}

int main(int argc, char **argv){

  /* this is executed on the CPU */
  printf("Hello world from CPU (HOST) \n");

  /* queue up kernel  on DEVICE */
  helloWorld <<< 2, 32 >>> ();

  /* sync DEVICE and HOST */
  cudaDeviceSynchronize();
  
}
