
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

/* 

to compile: 

gcc -o hw14 hw14.c -fopenmp -lm

to run with 7 threads: 

./hw14 7

*/

void printNorm(const char *message, int N, float *x){

  // compute and print Euclidean norm of N entries from x
  float res = 0;
  int n;
  for(n=0;n<N;++n){
    res += x[n]*x[n];
  }
  res = sqrt(res);
  
  printf("%s %g\n", message, res); 
}

void q1(int N, float *x, float *y){

  int n;

  srand48(1234);
  /* 
     q1: 2 points
  */
  for(n=0;n<N;++n){
    x[n] = drand48();
    y[n] = drand48();
  }

}


void q2(int N, float *x, float *y, float *z){

  int n;

  /* 
     q2: 2 points
  */

  for(n=0;n<N;++n){
    z[n] = x[n] + y[n];
  }

}

void q3(int N, float *x, float *y, float *z){

  int n;

  /* 
     q3: 2 points
  */
  for(n=1;n<N;++n){
    z[n] = x[N-1-n]*y[n-1] + z[n]; 
  }

}

void q4(int N, float *x, float *y, float *z){

  int n;

  /* 
     q4: 2 points
  */
  for(n=1;n<N;++n){
    z[n] = x[n];
    y[n] = x[N-1-n];
  }

}

float q5(int N, float *x, float *y, float *z){

  int n;
  float sumx = 0;

  /*
     q5: 3 points 
  */
  for(n=0;n<N;++n){
    sumx += x[n];
  }

  return sumx;
}


void q6(int N, float *x, float *y, float *z){

  int n;

  /* 
     q6: 8 points
  */
  for(n=0;n<N-1;++n){
    x[n] +=  x[n+1];
  }
}

void q7(int N, float *x, float *y, float *z){

  int n;

  /* 
     q7: 8 extra credit points
  */
  for(n=1;n<N-1;++n){
    x[n] +=  (x[n+1] - y[n]);
    y[n]  =  0.5*(y[n+1] + y[n-1]);
    z[n] +=  (z[n-1] - x[n]);
  }

}


void q8(int N, float *x, float *y, float *z){

  int n;

  /* 
     q8: 8 points
  */
  x[0] = 0;
  x[1] = 1;
  for(n=2;n<N;++n){
    x[n] = x[n-1] + x[n-2];
  }

}


int main(int argc, char **argv){

  if(argc!=2){
    printf("usage: %s Nthreads\n", argv[0]);
    exit(-1);
  }
  
  int N = 100000;
  int Nthreads = atoi(argv[1]);
  
  float *x = (float*) calloc(N, sizeof(float));
  float *y = (float*) calloc(N, sizeof(float));
  float *z = (float*) calloc(N, sizeof(float));

  /* run the assignment questions with 1 thread*/

  q1(N, x, y);
  printNorm("omp - q1:", N, y);

  /* force subsequent functions to use serially generated random data */
  omp_set_num_threads(1);
  q1(N, x, y);
  printNorm("q1:", N, y);

  /* do all subsequent results with input number of threads */
  omp_set_num_threads(Nthreads);
  
  q2(N, x, y, z);
  printNorm("q2:", N, z);

  q3(N, x, y, z);
  printNorm("q3:", N, z);
  
  q4(N, x, y, z);
  printNorm("q4:", N, z);
  
  float sumx = q5(N, x, y, z);
  printf("q5: sumx=%g\n", sumx);
  
  q6(N, x, y, z);
  printNorm("q6:", N, x);

  /* these two are extra credit optional:
     q7(N, x, y, z);
     q8(N, x, y, z);
  */

  
  free(x);
  free(y);
  free(z);

  return 0;
}
  
