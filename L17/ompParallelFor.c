
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv){

  omp_set_num_threads(20);

  int n;
  int N = 1000;
  int *array = (int*) calloc(N, sizeof(int));

  int j = 1234;
  
  array[0] = 999;
#pragma omp parallel firstprivate(j)
  {
#pragma omp for
    for(n=N-1;n>=0;--n){
      // GOOD:
      //array[n] = n;
      // BAD:
      array[n] = array[n-1] + 1;
    }
  }
  
  for(int n=0;n<N;++n){
    printf("array[%d]=%d\n", n, array[n]);
  }
  

  return 0;
}
