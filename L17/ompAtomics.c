
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv){

  omp_set_num_threads(14);

  int i = 0, j = 0;

#pragma omp parallel reduction(+:i)
  {
    int n;
    for(n=0;n<10000;++n){
      ++i;

      if(n%193 == 0){
	/* protect  the increment from interruption */
#pragma omp atomic
	++j;
      }
	
    }
    
  }

  printf("i=%d\n", i);
  

  return 0;
}
