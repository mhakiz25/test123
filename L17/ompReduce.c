
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv){

  omp_set_num_threads(14);

  int i = 0;
  int hits = 0;
  int samples = 0;
#pragma omp parallel reduction(+:hits,samples)
  {
    int n;
    for(n=0;n<10000;++n){
      x,y = random point in unit box;
      if((x,y) is in the unit circle){
	hits = hits + 1;
      }
      samples= samples + 1;
    }
    
  }

  double piEstimate = 4.*(double)hits/samples;
  printf("i=%d\n", i);
  

  return 0;
}
