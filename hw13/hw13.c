
#include <stdio.h>
#include <stdlib.h>

void q1(int N, float *x, float *y){

  int n;

  /* 
     q1: 3 points
     q1.a: is this a natural candidate to make parallel ? please explain.
     q1.b: is it easy to make this parallel and to exactly reproduce the serial version ?
     q1.c: which OpenMP construct would you use here ?
  */
  for(n=0;n<N;++n){
    x[n] = drand48();
    y[n] = drand48();
  }

}


void q2(int N, float *x, float *y, float *z){

  int n;

  /* 
     q2: 3 points
     q2.a: is this a natural candidate to make parallel ? please explain.
     q2.b: will a parallel version exactly reproduce the serial version ?
     q2.c: which OpenMP construct would you use here ?
  */
  for(n=0;n<N;++n){
    z[n] = x[n] + y[n];
  }

}

void q3(int N, float *x, float *y, float *z){

  int n;

  /* 
     q3: 3 points
     q3.a: is this a natural candidate to make parallel ? please explain .
     q3.b: is it easy to make this parallel and to exactly reproduce the serial version ?
     q3.c: which OpenMP construct would you use here ?
  */
  for(n=1;n<N;++n){
    z[n] = x[N-1-n]*y[n-1] + z[n]; 
  }

}

void q4(int N, float *x, float *y, float *z){

  int n;

  /* 
     q4: 2 points
     q4.a: is this a natural candidate to make parallel ? please explain.
     q4.b: which OpenMP construct would you use here ?
  */
  for(n=1;n<N;++n){
    z[n] = x[n];
    y[n] = x[N-1-n];
  }

}

float q5(int N, float *x, float *y, float *z){

  int n;
  float sumx = 0;

  /*
     q5: 2 points 
     q5.a: is this a natural candidate to make parallel ? 
     q5.b: which OpenMP construct would you use here ?
  */
  for(n=0;n<N;++n){
    sumx += x[n];
  }

  return sumx;
}


void q6(int N, float *x, float *y, float *z){

  int n;

  /* 
     q6: 3 points
     q6.a: is this a natural candidate to make parallel ? 
     q6.b: draw a diagram (pen and paper, or digital) to illustrate any difficulties making this parallel
     q6.c: suggest a way forward to make this thread parallel.
  */
  for(n=0;n<N-1;++n){
    x[n] +=  x[n+1];
  }
}

void q7(int N, float *x, float *y, float *z){

  int n;

  /* 
     q7: 4 points
     q7.a: is this a natural candidate to make parallel ? 
     q7.b: draw a diagram (pen and paper, or digital) to illustrate any loop carried dependencies.
     q7.c: discuss the complications of making this parallel.
     q7.d: suggest a code refactor that makes this a better candidate for making parallel.
  */
  for(n=1;n<N-1;++n){
    x[n] +=  (x[n+1] - y[n]);
    y[n]  =  0.5*(y[n+1] + y[n-1]);
    z[n] +=  (z[n-1] - x[n]);
  }

}


void q8(int N, float *x, float *y, float *z){

  int n;

  /* 
     q8: 3 points
     q8.a: what is the name of the recurrence that this implements ?
     q8.b: is there a loop carried dependence ?
     q8.c: is this a good candidate for making parallel ?
  */
  x[0] = 0;
  x[1] = 1;
  for(n=2;n<N;++n){
    x[n] = x[n-1] + x[n-2];
  }

}


int main(int argc, char **argv){

  int N = 100000;
  float *x = (float*) calloc(N, sizeof(float));
  float *y = (float*) calloc(N, sizeof(float));
  float *z = (float*) calloc(N, sizeof(float));

  /* run the assignment questions */
  q1(N, x, y);
  q2(N, x, y, z);
  q3(N, x, y, z);
  q4(N, x, y, z);

  float sumx = q5(N, x, y, z);

  q6(N, x, y, z);
  q7(N, x, y, z);
  q8(N, x, y, z);
  
  free(x);
  free(y);
  free(z);

  return 0;
}
  
