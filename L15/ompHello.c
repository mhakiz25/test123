
#include <stdio.h>
#include <omp.h>

/* to compile:

gcc -fopenmp -o ompHello ompHello.c

*/

int main(int argc, char **argv){

  int n = 10000;

#pragma omp parallel num_threads(8)
  {

    int threadNumber = omp_get_thread_num();

    if(threadNumber%2 ==0){
      printf("Hello world from even thread %d!\n",
	     threadNumber);
    }

    if(threadNumber%2 ==1){
      printf("Odd threads rule !!!\n");
    }

    n = threadNumber;
    
  }

  printf("n=%d\n", n);

}
