#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* 

1. to build: 

gcc -o modBitPrinter modBitPrinter.c

2. to run
./modBitPrinter value

3. example

./modBitPrinter 1.0078125
0|01111111|00000010000000000000000

*/


void print_binary(uint32_t num) {
  for (int i = 31; i >= 0; i--) {
    
    if(i==30 || i==22)
       printf("|");
    
    printf("%d", (num >> i) & 1);
  }
  printf("\n");
}

int main(int argc, char **argv) {

  if(argc<2){
    printf("usage: modBitPrinter value\n");
    exit(-1);
  }
  float input = atof(argv[1]);
  
  uint32_t* input_bits = (uint32_t*) &input;
  print_binary(*input_bits);
  
  return 0;
}
