#include <stdio.h>
#include <stdint.h>

void print_binary(uint32_t num) {
    for (int i = 31; i >= 0; i--) {
        printf("%d", (num >> i) & 1);
    }
    printf("\n");
}

int main() {
    float input;
    printf("Enter a single precision floating point number: ");
    scanf("%f", &input);

    uint32_t* input_bits = (uint32_t*) &input;
    print_binary(*input_bits);

    return 0;
}
