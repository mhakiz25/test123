#include <stdio.h>
#include <math.h>
#include <string.h>

void print_binary(double x) {
    unsigned long long *bits = (unsigned long long*)&x;
    char binary[65];
    binary[64] = '\0';
    for (int i = 63; i >= 0; i--) {
        if (*bits & ((unsigned long long)1 << i)) {
            binary[63-i] = '1';
        } else {
            binary[63-i] = '0';
        }
    }
    printf("The binary pattern of %lf is %s\n", x, binary);
}

int main() {
    double num;
    printf("Enter a double precision floating-point number: ");
    scanf("%lf", &num);
    print_binary(num);
    return 0;
}
