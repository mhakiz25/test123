#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double fa(int N, double x){

  double fax = 0;
  int n;
  for(n=0;n<=N;++n){
    fax += pow(-1.,n)*pow(x,2*n)/exp(gamma(2*n+1.));
  }
  return fax;
}

double faV2(int N, double x){

  double fax = 0;
  int n;
  double res = 1;
  for(n=0;n<=N;++n){
    fax += res;
    res *= -1*x*x/( (2*n+1)*(2*n+2) );
  }
  return fax;
}


double fb(int N, double x){

  double fbx = 0;
  int n;
  for(n=0;n<=N;++n){
    fbx += pow(x,n);
  }
  return fbx;
}

double fc(int N, double x){

  double fcx = 0;
  int n;
  for(n=1;n<=N;++n){
    fcx += pow(-1.,n+1)*pow(x,n)/n;
  }
  return fcx;
}

double fd(int N, double x){

  double fdx = ( (N+x)-N)/(x);
  return fdx;
}

double fe(int N, double x){

  double fex = 0;
  int n;
  for(n=0;n<=N;++n){
    fex += pow(x,n)/exp(gamma(n+1));
  }
  return fex;
}

double feV2(int N, double x){

  double fex = 0;
  int n;
  double res = 1;
  for(n=0;n<=N;++n){
    fex += res;
    res *= x/(n+1);
  }
  return fex;
}


int main(int argc, char **argv){

  double xa = 1.5707963268;
  double xb = 0.2;
  double xc = 0.3;
  double xd = 1e-6;
  double xe = 1.;
  
  int Na = 100;
  double vala = fa(Na,xa);
  double refvala = cos(xa);

  int Nb = 10;
  double valb = fb(Nb, xb);
  double refvalb = (pow(xb, Nb+1)-1)/(xb-1.);

  int Nc = 31;
  double valc = fc(Nc,xc);
  double refvalc = log(1.+xc);

  int Nd = 1000000;
  double vald = fd(Nd,xd);
  double refvald = 1;
  
  int Ne = 100;
  double vale = fe(Ne,xe);
  double refvale = exp(xe);

  
  double valaV2 = faV2(Na,xa);
  double valeV2 = feV2(Ne,xe);

  printf("fn, eval, ref, evalV2\n");
  printf("fa, % 17.15le, %17.15le, %17.15le\n", vala, refvala, valaV2);
  printf("fb, % 17.15le, %17.15le, ------\n", valb, refvalb);
  printf("fc, % 17.15le, %17.15le, ------\n", valc, refvalc);
  printf("fd, % 17.15le, %17.15le, ------\n", vald, refvald);
  printf("fa, % 17.15le, %17.15le, %17.15le\n", vale, refvale, valeV2);

  return 0;
}

